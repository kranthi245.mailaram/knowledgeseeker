import React, { Component } from "react";
import PropTypes from "prop-types";
import { Platform, StyleSheet, Text, TextInput, View } from "react-native";



const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    width: '100%',
    borderColor:'#eee',
   
},
  fieldLabel : {
    marginLeft : 10,
    padding: 5,
    paddingBottom: 0,
    color: '#333',
    fontSize: 17,
    fontWeight: '700',
    width: '100%',
  },

  textInput : {
    height : 40, 
    marginLeft : 10, 
    width : "96%", 
    marginBottom : 20,
    borderBottomWidth: 2,
    paddingRight: 5,
    paddingLeft: 5,
    paddingBottom: 2,
    color: '#333',
    fontSize: 18,
    
  }

 

});


class CustomTextInput extends Component {

  render() {

    const {
      label, labelStyle, maxLength, textInputStyle, stateHolder, stateFieldName
    } = this.props;

    return (
      <View style={styles.container}>
        <Text style={ [ styles.fieldLabel, labelStyle ] }>{label}</Text>
        <TextInput
          autoCorrect={false}
          maxLength={ maxLength }
          onChangeText={ (inText) => stateHolder.setState(
            () => {
              const obj = { };
              obj[stateFieldName] = inText;
              return obj;
            }
          ) }
          style={ [ styles.textInput, textInputStyle ] }
        />
      </View>
    );

  }

} 



 
CustomTextInput.propTypes = {

  label : PropTypes.string.isRequired,
  labelStyle : PropTypes.object,
  maxLength : PropTypes.number,
  textInputStyle : PropTypes.object,
  stateHolder : PropTypes.object.isRequired,
  stateFieldName : PropTypes.string.isRequired

}; 


export default CustomTextInput;
