import React from 'react';
import firebase from '@firebase/app'
import '@firebase/auth'
import '@firebase/database'

/*var firebase = require('firebase/app');
require('firebase/auth');
require('firebase/database');*/


const config ={
   
  apiKey: "AIzaSyB8P7gcwknBm27_nk0UZGRqZJLZkqgVdWk",
  authDomain: "bscproject-e47a5.firebaseapp.com",
  databaseURL: "https://bscproject-e47a5.firebaseio.com",
  projectId: "bscproject-e47a5",
  storageBucket: "bscproject-e47a5.appspot.com",
  messagingSenderId: "837012573393"

  };

export default !firebase.apps.length ?  firebase.initializeApp(config): firebase.app();
 