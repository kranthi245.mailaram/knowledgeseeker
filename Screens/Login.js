import React, {Component} from 'react';
import {ActivityIndicator, StyleSheet, Text, View,AsyncStorage} from 'react-native';
import  CustomButton from '../components/CustomButton';
import  {Input} from '../components/input';
import firebase from '../components/firebase'



export default class Login extends Component {
  constructor(props){
    super(props);

    this.state ={
    email: '',
    password: '',
    error:'',
    authenticating: false,
    logged: false,
  }
}



handleSignIn = () => {
  
 
  this.setState({error: '', authenticating: true});
  

  const{email, password}=this.state;
  firebase.auth().signInWithEmailAndPassword(email,password)
  .then(()=> {
    var TutorAcc = null;
    var user = firebase.auth().currentUser
    
    firebase.database().ref().child("Tutors/"+user.uid)
    .once("value",snapshot =>{
      TutorAcc= snapshot.val();

      if(TutorAcc!=null){
        this.setState({error: '', authenticating: false});
        this.props.navigation.navigate('SubjectScreen');
      }else{
        this.setState({error: '', authenticating: false});
        this.props.navigation.navigate('StudentScreen');
      }

    });
   
  })
  .catch((error) =>{
    var errorCode = error.code;
    this.errorHandler(errorCode);
    this.setState({error: 'Authentication failed', authenticating: false});
  });
}

errorHandler(errorCode){
  var errorName= errorCode.substring(5);
  switch(errorCode){
    case errorCode: alert(errorName); break;
    default: alert('Undefined errorCode');

  }
}

renderCurrentState(){
  if (this.state.authenticating){
    return (
      <View style={styles.form}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    );

  }

  return(
    <View style={styles.form}>
      <Input 
        placeholder="Enter your Email..."
        label="Email"
        onChangeText={ email => this.setState({email})}
        value={this.state.email}
      />

     
      <Input 
        placeholder="Enter your Password..."
        label="Password"
        secureTextEntry
        onChangeText={ password => this.setState({password})}
        value={this.state.password}
      />
    
      <CustomButton 
        width="94%"
        text="Log In"
        ButtonColor="#3cb371"
        onPress={()=> this.handleSignIn()}
      />
      {this.state.error ? <Text>{this.state.error}</Text> : null}
      
    </View>  
);
}


  
  render() {

    return (
      <View style={styles.container}>
        {this.renderCurrentState()}  
     
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#00aeef',
  },
  form: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  }
});

