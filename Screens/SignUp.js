import React, {Component} from 'react';
import {
  ActivityIndicator, 
  StyleSheet, 
  Switch,
  Text, 
  View} from 'react-native';
import  CustomButton from '../components/CustomButton';
import  {Input} from '../components/input';
import firebase from '../components/firebase'



export default class SignUp extends Component {
  constructor(props){
    super(props);

    this.state ={
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    repeatPassword: '',
    error:'',
    authenticating: false,
    tutorAcc: false,
  }
}




errorHandler(errorCode){
  var errorName= errorCode.substring(5);
  switch(errorCode){
    case errorCode: alert(errorName); break;
    default: alert('Undefined errorCode');

  }
}

handleSignUp(){
  this.setState({error: '', authenticating: true});
  
  if(this.state.password !== this.state.repeatPassword){
    this.setState({error: '', authenticating: false});
    alert("Password and repeated Password are not same");
    
  }
  else
  {
    const{email, password,firstName,lastName, tutorAcc}=this.state;
    console.log("TUTOR? "+tutorAcc);
    if(!tutorAcc){
      const newMessage = firebase.database().ref().child('Students/');
      firebase.auth().createUserWithEmailAndPassword( email, password)
      .then(()=> {
        let SubjectUID = firebase.auth().currentUser.uid;
        this.setState({error: '', authenticating: false});
        newMessage.child(SubjectUID+'/studentDetails').set({
          FirstName: firstName,
          LastName: lastName,
          StudentKEY: SubjectUID
        });
        this.props.navigation.navigate('StudentScreen',{
          TutorAcc: this.state.tutorAcc,
        })
        
      })
      .catch((error) =>{
        var errorCode = error.code;
        this.errorHandler(errorCode);
        this.setState({error: 'Authentication failed', authenticating: false});
      }); 
      
    } else if(tutorAcc){
      const newMessage = firebase.database().ref().child('Tutors/');
      firebase.auth().createUserWithEmailAndPassword( email, password)
      .then(()=> {
        let SubjectUID = firebase.auth().currentUser.uid;
        this.setState({error: '', authenticating: false});
        newMessage.child(SubjectUID+'/tutorDetails').set({
          FirstName: firstName,
          LastName: lastName,
          TutorKEY: SubjectUID
        });
        this.props.navigation.navigate('SubjectScreen',{
          TutorAcc: this.state.tutorAcc,
        })
        
      })
      .catch((error) =>{
        var errorCode = error.code;
        this.errorHandler(errorCode);
        this.setState({error: 'Authentication failed', authenticating: false});
      }); 

    }
  
  }
 
}



renderCurrentState(){
  if (this.state.authenticating){
    return (
      <View style={styles.form}>
        <Text>Loading</Text>
        <ActivityIndicator size="large" />
      </View>
    );

  }

  return(
    <View style={styles.form}>
      <Input 
        placeholder="Enter your First Name..."
        label="First Name"
        onChangeText={ firstName => this.setState({firstName})}
        value={this.state.firstName}
      />

      <Input 
        placeholder="Enter your Last Name..."
        label="Last Name"
        onChangeText={ lastName => this.setState({lastName})}
        value={this.state.lastName}
      />

      <Input 
        placeholder="Enter your Email..."
        label="Email"
        onChangeText={ email => this.setState({email})}
        value={this.state.email}
      />

     
      <Input 
        placeholder="Enter your Password..."
        label="Password"
        secureTextEntry
        onChangeText={ password => this.setState({password})}
        value={this.state.password}
      />

      <Input 
        placeholder="Repeat your Password..."
        label="Repeat your Password"
        secureTextEntry
        onChangeText={ repeatPassword => this.setState({repeatPassword})}
        value={this.state.repeatPassword}
      />
      
     
   
      
      <CustomButton 
        width="94%"
        text="Create Account"
        ButtonColor="#3cb371"
        onPress={()=> this.handleSignUp()}
      />
      
      <View style={styles.SwitchPos}>
       <Text style={{fontWeight : "bold", fontSize : 16}}>Tutor Account ? : </Text>
       <Switch value={ this.state.tutorAcc}
        onValueChange={(inValue)=>{
          
          this.setState({tutorAcc: inValue})}
        
        }
      />
      </View>
    </View>  
);
}


  
  render() {

    return (
      <View style={styles.container}>
        {this.renderCurrentState()}  
     
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#00aeef',
  },
  form: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    
  },
  SwitchPos:{
    alignItems: 'center',
    justifyContent:'flex-end',
    width:'94%',
    flexDirection: 'row',
  }
});