import React, {Component} from "react";
import CustomButton from "../components/CustomButton";
import CustomTextInput from "../components/CustomTextInput";
import {
  Alert, AsyncStorage, BackHandler, FlatList, Picker, Platform, ScrollView,
  StyleSheet, Text, View
} from "react-native";
import { Root, Toast } from "native-base";
import firebase from '../components/firebase'





const styles = StyleSheet.create({

  SubjectScreenContainer : {
    flex : 1,
    alignItems : "center",
    justifyContent : "flex-start",
    backgroundColor: '#c7cfcf',
  },
  SubjectScreenButtonContainer : {
    alignItems : "center",
    justifyContent : "center",
    flexDirection:"row",
  },

  SubjectList : {
    width : "94%"
  },

  SubjectContainer : {
    flexDirection : "column",
    marginTop : 4,
    marginBottom : 4,
    borderColor : "#e0e0e0",
    borderBottomWidth : 2,
    alignItems : "flex-start"
  },

  SubjectName : {
    flex : 1,
    fontSize: 14,
    fontWeight: 'bold',
  
  },
  
  SubjectRow : {
    flexDirection : "row",
    alignItems : "center",
    justifyContent: "space-between",
  
  },

  addScreenContainer : {
    marginTop : 10
  },

  addScreenInnerContainer : {
    flex : 1,
    alignItems : "center",
    paddingTop : 20,
    width : "100%"
  },

  addScreenFormContainer : {
    width : "96%"
  },

  fieldLabel : {
    marginLeft : 10
  },

  addScreenButtonsContainer : {
    flexDirection : "row",
    justifyContent : "center"
  }

});



class SubjectScreen extends Component {
  constructor(props) {

    super(props);

    this.state = {
      listData : [ ],
      Session: "",
    
    };
    this.DeleteSubject.bind(this);
    

  } 

  componentDidMount() {
    var user = firebase.auth().currentUser
    firebase.database().ref().child("Tutors/"+user.uid)
    .once("value",snapshot =>{
      const data = snapshot.val()
      
      
      if(snapshot.val()){
        const initMessages = [];
        Object
        .keys(data)
        .forEach((message) => {
        
          
          if((firebase.auth().currentUser.uid)!=(data[message].TutorKEY)) {
            initMessages.push(data[message])
          }
         
        });
        this.setState({
          listData: initMessages
        })
      }
    });

    firebase.database().ref().child("Tutors/"+user.uid)
    .on("child_added",snapshot => {
      const data = snapshot.val();
      if(data){
        this.setState(prevState => ({
          listData: [data, ...prevState.listData] 
        }))
      }
    })

    // Block hardware back button on Android.
    BackHandler.addEventListener(
      "hardwareBackPress", () => { return true; }
    );
    
  }; 

  DeleteSubject(key){
    var user = firebase.auth().currentUser
    firebase.database().ref().child("Tutors/"+user.uid+"/"+key)
    .remove();

  
    const newState= this.state.listData;

    for (var i = newState.length - 1; i >= 0; --i) {
      if (newState[i].KEY == key) {
          newState.splice(i,1);

          this.setState({listData: newState});
      }
    }   
  }


  



  render() { 
    
    return (

    <Root>
      <View style={styles.SubjectScreenContainer}>
        <View style={styles.SubjectScreenButtonContainer}>
          <CustomButton
            text="Add Subject"
            width="45%"
            ButtonColor="#3cb371"
            onPress={ () => { this.props.navigation.navigate("AddScreen"); } }
          />
          <CustomButton
            text="See Students"
            width="45%"
            ButtonColor="#3cb371"
            onPress={ () => { this.props.navigation.navigate("AddScreen"); } }
          />
        </View>
         
        <FlatList
          style={styles.SubjectList}
          data={this.state.listData}
          keyExtractor={item => item.KEY}
          
          renderItem={ ({item}) => {
            
            
            return(
              (!( "FirstName" in item))?(
              <View style={styles.SubjectContainer}>
              <View style = {{flexDirection: "row"}}>
                <View style={{flex: 0.75}}>
                  <View style={{flexDirection:"column"}}>
                    <View style={styles.SubjectRow}>
                      <Text style={styles.SubjectName}>Subject:</Text>
                      <Text style={styles.SubjectName}>{item.Subject}</Text>
                    </View>

                    <View style={styles.SubjectRow}>
                      <Text style={styles.SubjectName}>Teaching Time:</Text>
                      <Text style={styles.SubjectName}> {item.Time}</Text>
                    </View>

                    <View style={styles.SubjectRow}>
                      <Text style={styles.SubjectName}>Price per hour:</Text>
                      <Text style={styles.SubjectName}> {item.Rate}</Text>
                    </View>  

                    <View style={styles.SubjectRow}>
                      <Text style={styles.SubjectName}>Tel:</Text>
                      <Text style={styles.SubjectName}> {item.Number}</Text>
                    </View>
                  </View>
                </View>  
              <View style={{flex: 0.25}}>  
              <CustomButton
                text="Delete"
                buttonStyle={{
                   padding : 0, height : 50, borderRadius : 8, margin : 0,
                    width : "100%",
                    backgroundColor :
                       "#5f0404" 
                }
                }
               
                onPress={ () => { this.DeleteSubject(item.KEY); } }
              />  
              
              </View>  
            </View>
            </View>):
            (<View/>)
            )}
          
          }

          ListEmptyComponent={
            <View style={styles.SubjectContainer}>
              <View style = {{flexDirection: "row"}}>
                <View style={{flex: 0.75}}>
                  <View style={{flexDirection:"column"}}>
                  <Text style={{
                     flex : 1,
                     fontSize: 25,
                     fontWeight: 'bold',
                   
                  }}>No Subjects added. Please add subjects.</Text>
                  </View>
                </View>  
              <View style={{flex: 0.25}}>  
            
              </View>  
            </View>
            </View>
          }
            
          
        />
      </View>
    </Root>

  ); } 


  
 

  


} 



class AddScreen extends Component {


 
  constructor(props) {

    super(props);

    this.state = {
      SubjectName : "",
	    time:"",
      price : "",
      phone : "",
      itemsNumber: "",
      key : "",
    };
      this.addSubject.bind(this);
  } 


  addSubject(){
    
    var user = firebase.auth().currentUser;
    const newMessage = firebase.database().ref().child('Tutors/'+user.uid).push();
    const SubjectUID= newMessage.key;
    newMessage.set({
      Subject: this.state.SubjectName,
      Time: this.state.time,
      Rate : this.state.price,
      Number: this.state.phone,
      KEY: SubjectUID
    });
   
    this.props.navigation.navigate("SubjectScreen");
  
  }
 
  
  render() { return (

    <ScrollView style={styles.addScreenContainer}>
      <View style={styles.addScreenInnerContainer}>
        <View style={styles.addScreenFormContainer}>
         
          <CustomTextInput
            label="Subject Name"
            maxLength={20}
            stateHolder={this}
            stateFieldName="SubjectName"
          />    
        
          <CustomTextInput
            label="Teaching Time"
            maxLength={20}
            stateHolder={this}
            stateFieldName="time"
          />    
		      <CustomTextInput
            label="Price per hour"
            maxLength={20}
            stateHolder={this}
            stateFieldName="price"
          />  
          
          { /* ########## Phone ########## */ }
          <CustomTextInput
            label="Phone Number"
            maxLength={20}
            stateHolder={this}
            stateFieldName="phone"
          />
        
        <View style={styles.addScreenButtonsContainer}>
          <CustomButton
            text="Cancel"
            width="44%"
            ButtonColor="#3cb371"
            onPress={
              () => { this.props.navigation.navigate("SubjectScreen"); }
            }
          />
          <CustomButton
            text="Save"
            width="44%"
            ButtonColor="#3cb371"
            onPress={ ()=>{
              this.addSubject();
              
            
            }
              }
          />
        </View>
        </View>
      </View>
    </ScrollView>

  ); } 


} 



export {SubjectScreen, AddScreen};


