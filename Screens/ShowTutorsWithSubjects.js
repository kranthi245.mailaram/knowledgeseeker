import React, {Component} from "react";
import CustomButton from "../components/CustomButton";
import CustomTextInput from "../components/CustomTextInput";
import {
  Alert, AsyncStorage, BackHandler, FlatList, Picker, Platform, ScrollView,
  StyleSheet, Text, View, ActivityIndicator
} from "react-native";
import {ListItem, SearchBar} from 'react-native-elements';
import { Root, Toast } from "native-base";
import firebase from '../components/firebase'


export default class ShowTutorsWithSubjects extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      data: [],
      error: null,
    };

    this.arrayholder = [];
  }

  componentDidMount() {
    this.makeFirebaseRequest();
  }

  
  makeFirebaseRequest = () => {
    firebase.database().ref().child("Tutors")
    .once("value",snapshot =>{
      const data = snapshot.val()
    
      if(snapshot.val()){
        const initMessages = [];
        var tutors = [];
          
            
        Object
        .values(data)
        .forEach((subData) => {
          const Teacher = Object
          .values(subData);
         
          var TeacherName;
          var TeacherfamilyName;
           
          Teacher.map(function(item){
            
            if(( "FirstName" in item)){
              Teachername = item.FirstName;
              TeacherfamilyName = item.LastName;
              
            }
              
          });

          Teacher.map(function(item){       
            if(!( "FirstName" in item)){
              tutors.push({
                "subject": item.Subject,
                "number": item.Number,
                "price": item.Rate,
                "time": item.Time,
                "firstName": Teachername,
                "lastName": TeacherfamilyName 
              });                     
            }            
          });

          
          
        });

        this.arrayholder= tutors;

        this.setState({
          data: tutors
        })
      }
    });
    

    

   
  };


  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '90%',
          backgroundColor: '#CED0CE',
          marginLeft: '5%',
        }}
      />
    );
  };

  searchFilterFunction = text => {
    this.setState({
      value: text,
    });

    const newData = this.arrayholder.filter(item => {
      const itemData = `${item.subject.toUpperCase()} ${item.price} ${item.time}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    this.setState({
      data: newData,
    });

    this.setState({
      data: newData,
    });

  };

  renderHeader = () => {
    return (
      <SearchBar
        placeholder="Type Here..."
        lightTheme
        round
        onChangeText={text => this.searchFilterFunction(text)}
        autoCorrect={false}
        value={this.state.value}
      />
    );
  };

  render() {
    if (this.state.loading) {
      return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <ActivityIndicator />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
           
      <FlatList
          data={this.state.data}
          renderItem={({ item }) => (
            <ListItem
              //leftAvatar={{ source: { uri: item.picture.thumbnail } }}
              title={`${item.subject} ${item.firstName}`}
              subtitle={
                <View>
                  <Text>{`${item.price} `}</Text>
                  <Text>{`${item.time}`}</Text>


                </View>
              
              
              }
            />
          )}
          keyExtractor={item => item.firstName}
          ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
        />

      </View>
    );
  }
}