import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View, 
  Image,
} from 'react-native';
import HomeScreenPic from '../images/HomeScreenPic.png'
import  CustomButton from '../components/CustomButton';
import firebase from '../components/firebase'


export default class HomeScreen extends Component {
 
  render() {
    return (
      <View style={styles.container}>
        
        <View style={styles.titleContainer}>
        <Text style={{fontFamily: 'Arial, serif ',fontSize: 40}}>Knowledge Seeker</Text>
        </View>
          <Image source={HomeScreenPic}
          style={styles.imageContainer}
         
          />
       
        
        <View style={styles.ButtonContainer}>
        <CustomButton 
        width="30%"
        text="Sign Up"
        ButtonColor="#3cb371"
        onPress={()=> this.props.navigation.navigate('SignUp')}
      />
        <CustomButton 
        width="20%"
        text="Log In"
        ButtonColor="#3cb371"
        onPress={()=> this.props.navigation.navigate('Login')}
      />
      
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
  },
  titleContainer: {
    flex:0.20,
    textAlign: 'center',
    alignItems:'center',
    justifyContent: 'center',
    
  },
  
  imageContainer: {
    flex: 0.55,
    width: undefined,
    height: undefined,
    alignItems: 'stretch'

  },
  ButtonContainer: {
    flex: 0.25,
    alignItems:'flex-start',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  
});

