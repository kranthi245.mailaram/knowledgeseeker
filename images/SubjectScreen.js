import React, {Component} from "react";
import CustomButton from "../components/CustomButton";
import CustomTextInput from "../components/CustomTextInput";
import {
  Alert, AsyncStorage, BackHandler, FlatList, Picker, Platform, ScrollView,
  StyleSheet, Text, View
} from "react-native";
import { Root, Toast } from "native-base";
import firebase from '../components/firebase'





const styles = StyleSheet.create({

  SubjectScreenContainer : {
    flex : 1,
    alignItems : "center",
    justifyContent : "center",
  },
  SubjectScreenButtonContainer : {
    alignItems : "center",
    justifyContent : "center",
    flexDirection:"row",
  },

  SubjectList : {
    width : "94%"
  },

  SubjectContainer : {
    flexDirection : "row",
    marginTop : 4,
    marginBottom : 4,
    borderColor : "#e0e0e0",
    borderBottomWidth : 2,
    alignItems : "center"
  },

  SubjectName : {
    flex : 1
  },

  addScreenContainer : {
    marginTop : 10
  },

  addScreenInnerContainer : {
    flex : 1,
    alignItems : "center",
    paddingTop : 20,
    width : "100%"
  },

  addScreenFormContainer : {
    width : "96%"
  },

  fieldLabel : {
    marginLeft : 10
  },

  addScreenButtonsContainer : {
    flexDirection : "row",
    justifyContent : "center"
  }

});



class SubjectScreen extends Component {


  
  constructor(inProps) {

    super(inProps);

    this.state = {
      listData : [ ],
    
    };

  } 

  render() { return (

    <Root>
      <View style={styles.SubjectScreenContainer}>
        <View style={styles.SubjectScreenButtonContainer}>
        <CustomButton
          text="Add Subject"
          width="45%"
          ButtonColor="#3cb371"
          onPress={ () => { this.props.navigation.navigate("AddScreen"); } }
        />
        <CustomButton
          text="See Students"
          width="45%"
          ButtonColor="#3cb371"
          onPress={ () => { this.props.navigation.navigate("AddScreen"); } }
        />
      </View>
        <FlatList
          style={styles.SubjectList}
          data={this.state.listData}
          renderItem={ ({item}) =>
            <View style={styles.SubjectContainer}>
              <Text style={styles.SubjectName}>{item.SubjectName}</Text>
              <CustomButton
                text="Delete"
                ButtonColor="#3cb371"
                onPress={ () => {
                  Alert.alert(
                    "Please confirm",
                    "Are you sure you want to delete this subject?",
                    [
                      { text : "Yes", onPress: () => {
                        // Pull data out of storage.
                        AsyncStorage.getItem("Subjects",
                          function(inError, inSubjects) {
                            if (inSubjects === null) {
                              inSubjects = [ ];
                            } else {
                              inSubjects = JSON.parse(inSubjects);
                            }
                            // Find the right one to delete and splice it out.
                            for (let i = 0; i < inSubjects.length; i++) {
                              const Subject = inSubjects[i];
                              if (Subject.key === item.key) {
                                inSubjects.splice(i, 1);
                                break;
                              }
                            }
                            // Store updated data in storage.
                            AsyncStorage.setItem("Subjects",
                              JSON.stringify(inSubjects), function() {
                                // Set new state to update list.
                                this.setState({ listData : inSubjects });
                                // Show toast message to confirm deletion.
                                Toast.show({
                                  text : "Subject deleted",
                                  position : "bottom",
                                  type : "danger",
                                  duration : 2000
                                });
                              }.bind(this)
                            );
                          }.bind(this)
                        );
                      } },
                      { text : "No" },
                      { text : "Cancel", style : "cancel" }
                    ],
                    { cancelable : true }
                  )
              } } />
            </View>
          }
        />
      </View>
    </Root>

  ); } 


  
   componentDidMount() {
     const {navigation} = this.props;
    var user = firebase.auth().currentUser;

    if (user!= null) {
    const IsTutor= navigation.getParam('TutorAcc', null);
    if(IsTutor==true){
        firebase.database().ref('Tutors/'+user.uid).set({
          email: user.email,
        }).catch((error)=>{
            console.log(error);
        });
      }
    }
    

    // Block hardware back button on Android.
    BackHandler.addEventListener(
      "hardwareBackPress", () => { return true; }
    );

    
    AsyncStorage.getItem("Subjects",
      function(inError, inSubjects) {
        if (inSubjects === null) {
          inSubjects = [ ];
          console.log("Storage is null");
        } else {
          inSubjects = JSON.parse(inSubjects);
          console.log("Storage is not null");
        }
        this.setState({ listData : inSubjects });
      }.bind(this)
    );

  }; 


} 



class AddScreen extends Component {


 
  constructor(inProps) {

    super(inProps);

    this.state = {
      SubjectName : "",
	    time:"",
      price : "",
      phone : "",
      itemsNumber: "",
      key : `r_${new Date().getTime()}`
    };

  } 


  
  render() { return (

    <ScrollView style={styles.addScreenContainer}>
      <View style={styles.addScreenInnerContainer}>
        <View style={styles.addScreenFormContainer}>
         
          <CustomTextInput
            label="Subject Name"
            maxLength={20}
            stateHolder={this}
            stateFieldName="SubjectName"
          />    
        
          <CustomTextInput
            label="Teaching Time"
            maxLength={20}
            stateHolder={this}
            stateFieldName="time"
          />    
		  <CustomTextInput
            label="Price per hour"
            maxLength={20}
            stateHolder={this}
            stateFieldName="price"
          />  
          
          { /* ########## Phone ########## */ }
          <CustomTextInput
            label="Phone Number"
            maxLength={20}
            stateHolder={this}
            stateFieldName="phone"
          />
        
        <View style={styles.addScreenButtonsContainer}>
          <CustomButton
            text="Cancel"
            width="44%"
            ButtonColor="#3cb371"
            onPress={
              () => { this.props.navigation.navigate("SubjectScreen"); }
            }
          />
          <CustomButton
            text="Save"
            width="44%"
            ButtonColor="#3cb371"
            onPress={ () => {
              AsyncStorage.getItem("Subjects",
                function(inError, inSubjects) {
                  if (inSubjects === null) {
                    inSubjects = [ ];
                  } else {
                    inSubjects = JSON.parse(inSubjects);
                  }
                   inSubjects.push(this.state);
                  
                  AsyncStorage.setItem("Subjects",
                    JSON.stringify(inSubjects), function() {
                      
                      this.props.navigation.navigate("SubjectScreen");
                    
                    }.bind(this)
                  );
                }.bind(this)
              );
            } }
          />
        </View>
        </View>
      </View>
    </ScrollView>

  ); } 


} 



export {SubjectScreen, AddScreen};


