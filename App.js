import React, {Component} from 'react';
import { Text, View, YellowBox} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Login from './Screens/Login';
import HomeScreen from './Screens/HomeScreen';
import SignUp from './Screens/SignUp';
import {SubjectScreen, AddScreen} from './Screens/SubjectScreen';
import ShowTutorsWithSubjects from './Screens/ShowTutorsWithSubjects';
import _ from 'lodash';


YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn =message => {
    if (message.indexOf('Setting a timer') <= -1){
      _console.warn(message)
    }
}

const RootStack = createStackNavigator({
   Home: {screen: HomeScreen},
    Login: {screen: Login},
    SignUp:{screen: SignUp},
    SubjectScreen:{screen: SubjectScreen},
    AddScreen:{screen: AddScreen},
    StudentScreen: {screen: ShowTutorsWithSubjects}
  },
 
  {
    initialRouteName: 'Home',
    headerMode:'none',
    navigationOptions:{
      headerVisible: false,
    }
  }



);

const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}